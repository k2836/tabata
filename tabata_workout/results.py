import tkinter as tk

from tabata_workout.utilities import switch_frame


class ResultsLayout(tk.Frame):

    def __init__(self, master=None, cnf={}, **kw):
        self.round_data = kw.pop("round_data")
        super().__init__(master, cnf, **kw)

    def grid(self, cnf={}, **kw):
        heading_lbl = tk.Label(self, text="Results", font=("lucida", 36, "bold"))
        round_heading_lbl = tk.Label(self, text="Round", font=("lucida", 25))
        reps_heading_lbl = tk.Label(self, text="Repetitions", font=("lucida", 25))
        rep_total = sum([r.repetitions for r in self.round_data])
        total_reps_lbl = tk.Label(self, text=f"Total: {rep_total}", font=("lucida", 25, "bold"))

        heading_lbl.grid(row=0, column=0, columnspan=2, padx=40, pady=(25, 15))
        total_reps_lbl.grid(row=1, column=0, columnspan=2, pady=(0, 10))
        round_heading_lbl.grid(row=2, column=0, pady=(0, 10), padx=(30, 10))
        reps_heading_lbl.grid(row=2, column=1, pady=(0, 10), padx=(0, 25))

        font = ("lucida", 22)
        for i, r in enumerate(self.round_data, start=1):
            tk.Label(self, text=i, font=font).grid(row=i + 1, column=0, sticky="nesw")
            tk.Label(self, text=r.repetitions, font=font).grid(row=i + 1, column=1, pady=(0, 10), sticky="nesw")
        columns, rows = self.grid_size()
        from tabata_workout.menu import MenuLayout
        menu_btn = tk.Button(
            self,
            text="Menu",
            command=lambda: switch_frame(self, MenuLayout)
        )
        menu_btn.grid(row=rows + 1, column=0, columnspan=2, pady=(15, 30))
        super().grid(cnf, **kw)
