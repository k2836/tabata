import tkinter as tk

from tabata_workout.tabata import TabataFrame
from tabata_workout.video_window import VideoWindow


class TabataLayout(tk.Frame):

    def __init__(self, master=None, cnf={}, **kw):
        rep_counter = kw.pop("rep_counter")
        VideoWindow(master, rep_counter=rep_counter).grid(row=0, column=0)
        TabataFrame(master, rep_counter=rep_counter).grid(row=1, column=0)
        super().__init__(master, cnf, **kw)

