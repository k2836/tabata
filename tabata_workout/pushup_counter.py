import numpy as np

from tabata_workout.rep_counter import UpDownRepCounter


class PushupCounter(UpDownRepCounter):
    UP_THRESHOLD = 150
    DOWN_THRESHOLD = 110

    def get_angles(self, landmarks):
        # LEFT
        a1 = np.array([landmarks[11].x, landmarks[11].y])  # Shoulder
        b1 = np.array([landmarks[13].x, landmarks[13].y])  # Elbow
        c1 = np.array([landmarks[15].x, landmarks[15].y])  # Wrist
        # RIGHT
        a2 = np.array([landmarks[12].x, landmarks[12].y])  # Shoulder
        b2 = np.array([landmarks[14].x, landmarks[14].y])  # Elbow
        c2 = np.array([landmarks[16].x, landmarks[16].y])  # Wrist

        left_arm_angle = self.calculate_angle(a1, b1, c1)
        right_arm_angle = self.calculate_angle(a2, b2, c2)
        return left_arm_angle, right_arm_angle
