import numpy as np

from tabata_workout.rep_counter import UpDownRepCounter


class SquatCounter(UpDownRepCounter):
    UP_THRESHOLD = 150
    DOWN_THRESHOLD = 110

    def get_angles(self, landmarks):
        # LEFT
        a1 = np.array([landmarks[23].x, landmarks[23].y])  # Shoulder
        b1 = np.array([landmarks[25].x, landmarks[25].y])  # Elbow
        c1 = np.array([landmarks[27].x, landmarks[27].y])  # Wrist
        # RIGHT
        a2 = np.array([landmarks[24].x, landmarks[24].y])  # Shoulder
        b2 = np.array([landmarks[26].x, landmarks[26].y])  # Elbow
        c2 = np.array([landmarks[28].x, landmarks[28].y])  # Wrist

        left_arm_angle = self.calculate_angle(a1, b1, c1)
        right_arm_angle = self.calculate_angle(a2, b2, c2)
        return left_arm_angle, right_arm_angle
