import threading
import time

import cv2.cv2 as cv2

from tabata_workout.utilities import FrequencyLimiter


class ThreadCamera:
    def __init__(self, rep_counter):
        self.fps_limit = 60
        self.fps = 0
        self.capture = cv2.VideoCapture(0)
        self.capture.set(cv2.CAP_PROP_BUFFERSIZE, 2)
        self.rep_counter = rep_counter
        self.thread_flag = False

    def start_recording(self):
        thread = threading.Thread(target=self._update_frame, args=())
        thread.daemon = True
        self.thread_flag = True
        thread.start()
        self._load_frame()

    def stop_recording(self):
        self.thread_flag = False
        time.sleep(1)
        self.capture.release()

    def get_fps(self):
        return self.fps

    def get_frame(self):
        frame = cv2.flip(self.frame, 1)
        return frame

    def _process_frame(self):
        while True:
            self.processed_frame = self.rep_counter.run(self.frame)

    def _update_frame(self):
        while self.thread_flag and self.capture.isOpened():
            with FrequencyLimiter(self.fps_limit) as fl:
                status, frame = self.capture.read()
                assert status, "Selected video capturing device is not available"
                # frame.flags.writeable = False
                if status and self.rep_counter:
                    frame = self.rep_counter.process(frame)
            self.fps = fl.measured_hz
            frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            self.frame = frame

    def _load_frame(self):
        """ Wait for the thread to load first frame """
        while True:
            time.sleep(0.01)
            if hasattr(self, "frame"):
                return

    def __enter__(self):
        self.start_recording()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.stop_recording()

    def __del__(self):
        if self.capture.isOpened():
            self.stop_recording()
