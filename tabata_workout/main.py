import tkinter as tk
from tabata_workout.menu import MenuLayout


def main():
    root = tk.Tk()
    root.title("Tabata Workout")
    root.resizable(False, False)
    main_frame = MenuLayout(root)
    main_frame.grid(row=0, column=0)
    root.eval('tk::PlaceWindow . center')
    main_frame.master.mainloop()


if __name__ == "__main__":
    main()
