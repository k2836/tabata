import tkinter as tk

from tabata_workout.signpost import SignpostFrame
from tabata_workout.timer_config import TimerSettingsFrame


class MenuLayout(tk.Frame):

    def grid(self, cnf={}, **kw):
        signpost = SignpostFrame(self.master)
        signpost.grid(row=0, column=0)

        tc = TimerSettingsFrame(self.master)
        tc.grid(row=0, column=1)
        super().grid(cnf, **kw)
