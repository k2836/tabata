import json
import math
import os

import keras
import numpy as np

from tabata_workout import CODEBASE_ROOT


def get_model(model_name):
    model_folder_dir = os.path.join(CODEBASE_ROOT, "models", model_name)
    # Load Model
    model = keras.models.load_model(os.path.join(model_folder_dir, f"{model_name}.h5"))
    # Load Classes
    with open(os.path.join(model_folder_dir, "class_map.json"), "r") as f:
        model_class_map = json.loads(f.read())
    model_class_map = {int(k): v for k, v in model_class_map.items()}

    return model, model_class_map


def get_2d_angle(p1, p2, p3):
    rad = np.arctan2(p3.y - p2.y, p3.x - p2.x) - np.arctan2(p1.y - p2.y, p1.x - p2.x)
    return np.abs(np.rad2deg(rad))


def get_2d_distance(p1, p2):
    return math.hypot(p2.x - p1.x, p2.y - p1.y)
