import tkinter as tk
from tkinter import ttk

from PIL import ImageTk, Image

from tabata_workout.camera import ThreadCamera


class VideoWindow(tk.Frame):

    def __init__(self, master=None, cnf={}, **kw):
        self.camera = ThreadCamera(kw.pop("rep_counter"))
        super().__init__(master, cnf, **kw)

    def grid(self, cnf={}, **kw):
        self.camera.start_recording()

        video_window = ttk.Label(self.master)
        video_window.grid(row=0, column=0, pady=(0, 20))

        # fps_label = None
        fps_label = ttk.Label(text=self.camera.get_fps())
        fps_label.place(relx=0.0, rely=0.0, in_=video_window)

        self.render_scene(video_window, fps_label)
        super().grid(cnf, **kw)

    def render_scene(self, video_window, fps_label):
        # Video window
        tk_img = ImageTk.PhotoImage(image=Image.fromarray(self.camera.get_frame()))
        video_window._image_cache = tk_img  # Alternative: video_window.imgtk = tk_img
        video_window.configure(image=tk_img)
        # Fps label
        fps_label.config(text=self.camera.get_fps()) if fps_label else ...
        video_window.after(1, self.render_scene, video_window, fps_label)

    def close(self):
        self.camera.stop_recording()
