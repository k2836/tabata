import tkinter as tk
from tkinter import ttk

from tabata_workout.pushup_counter import PushupCounter
from tabata_workout.squat_counter import SquatCounter
from tabata_workout.tabata_layout import TabataLayout
from tabata_workout.utilities import switch_frame


class ModeMenu(tk.Frame):

    def grid(self, cnf={}, **kw):
        info_lbl = ttk.Label(self, text="Please select what type of exersice you want to perform")
        # squat_btn = ttk.Button(
        #     self,
        #     text="Squats",
        #     width=7,
        #     command=lambda: switch_frame(self, TabataLayout, rep_counter=SquatCounter())
        # )
        pushup_btn = ttk.Button(
            self,
            text="Push-ups",
            width=7,
            command=lambda: switch_frame(self, TabataLayout, rep_counter=PushupCounter())
        )

        info_lbl.grid(row=0, column=0, padx=30, pady=20, columnspan=5)
        # squat_btn.grid(row=1, column=1, padx=30, pady=(0, 20))
        pushup_btn.grid(row=1, column=1, pady=(0, 20), columnspan=3)
        super().grid(cnf, **kw)
