import tkinter as tk
from tkinter import ttk

from tabata_workout.mode_menu import ModeMenu
from tabata_workout.utilities import switch_frame


class SignpostFrame(tk.Frame):

    def grid(self, cnf={}, **kw):
        btn0 = ttk.Button(self, text="Start", width=7, default="active", command=lambda: switch_frame(self, ModeMenu))
        btn1 = ttk.Button(self, text="Guide", width=7)
        btn2 = ttk.Button(self, text="Quit", width=7, command=self.master.quit)

        btn0.grid(row=0, column=0, padx=30, pady=20)
        btn1.grid(row=1, column=0, padx=30, pady=(0, 20))
        btn2.grid(row=2, column=0, padx=30, pady=(0, 20))
        super().grid(cnf, **kw)
