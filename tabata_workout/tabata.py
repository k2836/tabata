import json
import tkinter as tk
from enum import Enum
from tkinter import ttk

from tabata_workout.results import ResultsLayout
from tabata_workout.utilities import switch_frame


class Round:

    def __init__(self):
        self.repetitions = 0
        self.finished = False

    def increment_reps(self):
        self.repetitions += 1


class TabataStage(Enum):
    PREPARING = 0
    ROUND_START = 1
    WORKING_OUT = 2
    RESTING = 3
    FINISHED = 4


class TabataFrame(tk.Frame):

    def __init__(self, master=None, cnf={}, **kw):
        with open("conf.json", "r") as f:
            conf = json.load(f)

        self.round_length = conf["round_length"]
        self.rest_length = conf["rest_length"]
        self.rounds = [Round() for _ in range(conf["round_count"])]
        self.round_i = 0
        self.stage = TabataStage.PREPARING
        self.counter = kw.pop("rep_counter")

        super().__init__(master, cnf, **kw)

        from tabata_workout.menu import MenuLayout
        self.menu_btn = tk.Button(self, text="Back to Menu", command=lambda: switch_frame(self, MenuLayout))
        self.countdown_lbl = tk.Label(self, text="Get Ready", font=("lucida", 72, "bold"), width=25)
        self.round_lbl = tk.Label(self, text=f"Round: 1/{len(self.rounds)}", font=("lucida", 32), foreground="gray")
        self.count_lbl = tk.Label(self, text="Repetitions: 0", font=("lucida", 32), foreground="gray")
        self.results_btn = ttk.Button(
            self,
            text="Results",
            default="active",
            command=lambda: switch_frame(self, ResultsLayout, round_data=self.rounds)
        )

    def grid(self, cnf={}, **kw):
        self.countdown_lbl.grid(row=0, column=0, pady=(15, 10), columnspan=5)
        self.count_lbl.grid(row=1, column=2, pady=(0, 10))
        self.round_lbl.grid(row=2, column=2, pady=(0, 10))
        self.menu_btn.grid(row=3, column=2, pady=(10, 30))
        self.count_label_loop()
        self._hub_execute()
        super().grid(cnf, **kw)

    def _hub_execute(self):
        match self.stage:
            case TabataStage.PREPARING:
                self.prepare_window()
            case TabataStage.ROUND_START:
                self.start_round()
            case TabataStage.WORKING_OUT:
                self.countdown_timer(self.round_length)
            case TabataStage.RESTING:
                self.countdown_timer(self.rest_length)
            case TabataStage.FINISHED:
                self.render_final_menu()

    def prepare_window(self):
        self.stage = TabataStage.ROUND_START
        self.after(3000, self._hub_execute)

    def start_round(self):
        self.counter.resume_count()
        self.stage = TabataStage.WORKING_OUT
        self.countdown_lbl.config(text="Go!")
        self.after(700, self._hub_execute)

    def count_label_loop(self):
        self.count_lbl.config(text=f"Repetitions: {self.counter.count}")
        self.count_lbl.after(100, self.count_label_loop)

    def render_final_menu(self):
        self.countdown_lbl.config(text="Finished")
        self.menu_btn.grid(row=3, column=1, sticky="e")
        self.results_btn.grid(row=3, column=3, sticky="w")

    def countdown_timer(self, seconds):
        assert self.stage in (TabataStage.WORKING_OUT, TabataStage.RESTING)
        if seconds > 0:
            self.countdown_lbl.config(text=seconds)
            self.after(1000, self.countdown_timer, seconds - 1)
        elif self.stage == TabataStage.WORKING_OUT:
            # End of workout timer
            self.counter.pause_count()
            self.current_round.repetitions = self.counter.count
            self.current_round.finished = True
            is_last_round = self.current_round == self.rounds[-1]
            if is_last_round:
                self.stage = TabataStage.FINISHED
                self._hub_execute()
            else:
                self.stage = TabataStage.RESTING
                self.countdown_lbl.config(text="Rest!")
                self.after(700, self._hub_execute)
        elif self.stage == TabataStage.RESTING:
            # End of resting timer
            self.counter.reset_count()
            self.stage = TabataStage.ROUND_START
            self.round_i += 1
            self.round_lbl.config(text=f"Round: {self.round_i + 1}/{len(self.rounds)}")
            self._hub_execute()

    @property
    def current_round(self):
        return self.rounds[self.round_i]
