from collections import deque
from enum import Enum

import mediapipe as mp
import numpy as np
from cv2 import cv2

from tabata_workout import utils


class PositionStates(Enum):
    DOWN = 0
    UP = 1


class UpDownRepCounter:
    UP_THRESHOLD: int
    DOWN_THRESHOLD: int

    def __init__(self):
        self.positions = deque([None, None], maxlen=2)
        self.count = 0
        self.pose = mp.solutions.pose.Pose(
            min_detection_confidence=0.5,
            min_tracking_confidence=0.5
        )
        self.mp_drawing = mp.solutions.drawing_utils
        self.mp_drawing_styles = mp.solutions.drawing_styles
        self.count_flag = False
        self.model, self.model_class_map = utils.get_model("push_up_count_model")

    def process(self, image):
        image_landmarks = self._get_prediction(image)

        image = self.visualise_landmarks(image, image_landmarks)
        if not self.count_flag or not image_landmarks or not image_landmarks.pose_landmarks:
            return image
        image = self.visualise_landmarks(image, image_landmarks)

        plmx = image_landmarks.pose_landmarks.landmark
        re_angle = int(utils.get_2d_angle(plmx[16], plmx[14], plmx[12]))
        le_angle = int(utils.get_2d_angle(plmx[15], plmx[13], plmx[11]))
        rw_rs_prox = int(utils.get_2d_distance(plmx[16], plmx[12])*100)
        lw_ls_prox = int(utils.get_2d_distance(plmx[15], plmx[11])*100)
        model_input = np.expand_dims(np.array([re_angle, le_angle, rw_rs_prox, lw_ls_prox]), axis=0)
        prediction_vector = self.model.predict(model_input)
        pred = np.argmax(prediction_vector)
        pred_verbose = self.model_class_map[pred]

        if pred_verbose.lower() == "up":
            print("UP")
            if self.last_position is None:
                self.positions.append(PositionStates.UP)
            elif self.last_position == PositionStates.DOWN:
                if all(self.positions):
                    self.count += 1
                self.positions.append(PositionStates.UP)
        elif pred_verbose.lower() == "down":
            print("down")
            if self.last_position is None:
                self.positions.append(PositionStates.DOWN)
            elif self.last_position == PositionStates.UP:
                self.positions.append(PositionStates.DOWN)
        else:
            print("TRANSITIONING")

        return image

    def visualise_landmarks(self, image, landmarks):
        self.mp_drawing.draw_landmarks(
            image,
            landmarks.pose_landmarks,
            mp.solutions.pose.POSE_CONNECTIONS,
            landmark_drawing_spec=self.mp_drawing_styles.get_default_pose_landmarks_style())
        return image

    @property
    def last_position(self):
        return self.positions[1]

    def _get_prediction(self, image):
        image.flags.writeable = False
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        return self.pose.process(image)

    def reset_count(self):
        self.count = 0

    def pause_count(self):
        self.count_flag = False

    def resume_count(self):
        self.count_flag = True

    def get_angles(self, landmarks):
        raise NotImplemented

    def calculate_angle(self, a, b, c):
        """ Calculates angle between 3 2D points """
        radian = np.arctan2(c[1] - b[1], c[0] - b[0]) - np.arctan2(a[1] - b[1], a[0] - b[0])
        angle = np.abs(radian * 180.0 / np.pi)
        return angle
