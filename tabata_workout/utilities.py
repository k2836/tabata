import time


class FrequencyLimiter:
    def __init__(self, hz):
        self.target_hz = hz
        self.start = None
        self.measured_hz = None

    def __enter__(self):
        self.start = time.time()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        exec_time = time.time() - self.start
        sleep_time = (1 / self.target_hz) - exec_time
        time.sleep(sleep_time if sleep_time >= 0 else 0)
        end_time = time.time()
        self.measured_hz = float(format(1 / (end_time - self.start), ".1f"))


def switch_frame(active_frame, next_frame, **kwargs):
    master = active_frame.master
    for widget in active_frame.master.winfo_children():
        # widget.pack_forget()
        widget.destroy()
    # Display new layout
    new_layout = next_frame(master, **kwargs)
    new_layout.grid(row=0, column=0)
    master.eval('tk::PlaceWindow . center')
