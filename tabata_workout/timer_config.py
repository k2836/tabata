import json
import tkinter as tk
from tkinter import ttk


class TimerSettingsFrame(tk.Frame):
    DEFAULT_SETTINGS = {
        "round_count": 8,
        "round_length": 30,
        "rest_length": 10
    }

    def __init__(self, master=None, cnf={}, **kw):
        with open("conf.json", "r") as f:
            self.config = json.loads(f.read())
        self.config = self.DEFAULT_SETTINGS | self.config
        super().__init__(master, cnf, **kw)

    def grid(self, cnf={}, **kw):
        self._create_settings_component(0, "Rounds: ", self.config["round_count"], 1, 25, "round_count")
        self._create_settings_component(1, "Round length:", self.config["round_length"], 10, 60, "round_length")
        self._create_settings_component(2, "Rest time:", self.config["rest_length"], 5, 30, "rest_length")
        super().grid(cnf, **kw)

    def _create_settings_component(self, row, label_name, default_val, bottom_limit, upper_limit, settings_name):
        lbl = ttk.Label(self, text=label_name)
        val_lbl = tk.Label(self, text=default_val, width=2, background="white", name=settings_name)
        minus_btn = tk.Button(self, text="-", command=lambda: self._minus_command(val_lbl, bottom_limit))
        plus_btn = tk.Button(self, text="+", command=lambda: self._plus_command(val_lbl, upper_limit))
        col_len = self.size()[1]
        y_pad = (0, 20) if col_len else 20

        lbl.grid(row=row, column=0, padx=(30, 5), pady=y_pad, sticky="news")
        val_lbl.grid(row=row, column=1, padx=(0, 10), pady=y_pad, sticky="w")
        minus_btn.grid(row=row, column=2, pady=y_pad)
        plus_btn.grid(row=row, column=3, pady=y_pad, padx=(0, 30))

    def _plus_command(self, lbl, limit):
        int_val = int(lbl["text"])
        if int_val < limit:
            new_val = int_val + 1
            key = lbl._name
            self.config[key] = new_val
            with open("conf.json", "w") as f:
                json.dump(self.config, f)
            lbl.config(text=new_val)

    def _minus_command(self, lbl, limit):
        int_val = int(lbl["text"])
        if int_val > limit:
            new_val = int_val - 1
            key = lbl._name
            self.config[key] = new_val
            with open("conf.json", "w") as f:
                json.dump(self.config, f)
            lbl.config(text=new_val)
